# Journée Académique Éléa

Ce dépôt est le contenu du site associé à la journée académique moodle du 5 mars:
[https://drane-grenoble.forge.apps.education.fr/jaime/](https://drane-grenoble.forge.apps.education.fr/jaime/)
Cosntruit avec MKdocs avec le thème material et les plug-ins Awesome-pages et Search


## Construction du site :
### Live Preview  
mkdocs serve --watch-theme

### Build and publish  
mkdocs build

