title: Accueil
template: jam-theme/home.html
description: "Une journée pour découvrir et échanger autour de ressources et des usages, sous le thème de la e-éducation."
img: /jaime/images/banniere_jam.png
