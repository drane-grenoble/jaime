---
template: jam-theme/jam.html
title: LudiMoodle, ludification adaptative comme levier de réussite
description: Dans le contexte actuel de questionnement des pratiques pédagogiques, la démarche de LudiMoodle+ a pour objectif de développer, à travers la ludification adaptative, des leviers innovants. Les élèves ont changé et les méthodes traditionnelles d’enseignement fonctionnent plus difficilement aujourd’hui. L’intégration d’éléments ludiques à ces environnements peut jouer le rôle de levier motivationnel dans les apprentissages scolaires. Tout l’enjeu est d’associer aux ressources numériques pédagogiques des éléments ludiques adaptés aux besoins et motivations des élèves.
presentation: <iframe width="560" height="380" src="/jaime/diaporamas/ludimoodle.pdf" frameborder="0" allow="fullscreen"></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Kz72F0of9PM?si=hAA0IiqoWQED5oKZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
img: https://ecp.univ-lyon2.fr/medias/photo/vignet-ludimoodle_1682773506781-jpg
thumb: https://ecp.univ-lyon2.fr/medias/photo/vignet-ludimoodle_1682773506781-jpg
anim: <b>Pascal MÉRIAUX</b><br/>Chargé de projets<br/>DRANE Ac-Lyon
horaire: <b>15H30</b>&nbsp;&nbsp;&nbsp;salle 211
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
