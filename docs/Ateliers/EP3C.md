---
template: jam-theme/jam.html
title: Comportement-Cerveau-Cognition à l'ère de la transition numérique
description: Au cours de cet atelier seront présentés deux projets menés dans l’académie de Clermont-Ferrand &#58; le projet « e.P3C » et, dans son prolongement, le projet « e.P3C Transfert ». <br/>Dans cette aventure e.P3C (Pluralité des contextes, compétences et comportements), la gestion de l’hétérogénéité scolaire a été centrale. Les technologies numériques ont été utilisées avec les élèves pour diversifier les contextes d’apprentissage et les modes d’accès au savoir. <br/>Le projet e.P3C, déployé auprès de plus de 8 000 élèves, a déjà donné lieu à un recueil de 4 millions de données, pour tester expérimentalement l’efficacité des technologies numériques. La stratégie pédagogique numérique coconstruite et scénarisée avec les enseignants au sein d’un système de tutorat intelligent (STI) devrait permettre de réduire les inégalités ancrées dans l’origine sociale de leurs élèves.<br/>En prolongement, la formation issue du projet e.P3C Transfert a été élaborée afin de permettre aux enseignants stagiaires de construire et mettre en œuvre des scénarios pédagogiques permettant de différencier les apprentissages en s’appuyant sur l’utilisation du LMS Moodle-ÉLÉA dans la classe.
presentation: <iframe width="560" height="380" src="/jaime/diaporamas/eP3C.pdf" frameborder="0" allow="fullscreen"></iframe>
img: /jaime/images/memorisation.jpg
thumb: /jaime/images/memorisation.jpg
anim: <b>Christine BIDEUX</b><br/>Chargé de projets<br/>DRANE Ac-Clermont
horaire: <b>14H30</b>&nbsp;&nbsp;&nbsp;salle 211
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
