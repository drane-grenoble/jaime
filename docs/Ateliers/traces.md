---
template: jam-theme/jam.html
title: Suivre le travail des élèves, plus-value pédagogique et exemples
description: Éléa offre de nombreuses possibilités pour suivre le travail des élèves.<br/>Quelles plus-values pédagogiques peuvent être apportées par ce suivi ?<br/>Quels sont les outils disponibles ?<br/>Des exemples concrets permettront d'apporter des éléments de réponses à ces deux questions.
presentation: <iframe title="JAIME Éléa, le suivi du travail des élèves" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/0037f400-1e76-4db2-911d-1b39978d6be1" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe><iframe width="560" height="315" src="https://codimd.apps.education.fr/p/hXUQGHz4G#" frameborder="0" allowfullscreen></iframe>
img: /jaime/images/traces.jpg
thumb: /jaime/images/traces.jpg
anim: <b>Céline CHAMBON</b><br/>Formatrice ÉLÉA<br/>Professeure de Physique-Chimie
horaire: <b>14H30</b>&nbsp;&nbsp;&nbsp;amphi
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
