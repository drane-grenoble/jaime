---
template: jam-theme/jam.html
title: Engagement des apprenants dans une démarche métacognitive grâce à l’expression des Degrés de Certitude
img: /jaime/images/metacognition.jpg
thumb: /jaime/images/metacognition.jpg
description: Les Questionnaires à Choix Multiple (QCM) permettent d'évaluer les connaissances des élèves fréquemment et avec un feedback automatique et immédiat. Cependant, ils sont souvent associées à des savoirs découpés, centrés sur des objectifs de mémorisation.<br/>Or il est possible de les améliorer pour aborder des objectifs de haut niveau cognitif et pour favoriser le développement de la métacognition. Parmi les méthodes qui le permettent, nous vous proposons de détailler le recueil et l’exploitation des degrés de certitude des élèves, en discutant notamment &#58;<ul><li>des grands principes sous-jacents,</li><li>des différents feed-backs destinés à l’apprenant, mais également à l’enseignant,</li><li>des solutions techniques pour recueillir ces degrés de certitudes et fournir les feed-back.
anim: <b>Julien DOUADY</b><br/>Enseignants-chercheurs<br/>LIG – équipe MeTAH (UGA)<br/><br/><b>Christian HOFFMANN</b><br/>Enseignants-chercheurs<br/>LIG – équipe MeTAH (UGA)
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;salle 211
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
