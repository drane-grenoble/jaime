---
template: jam-theme/jam.html
title: Moodle pour les sportifs de haut-niveau du Pôle France Ski
description: Le Lycée Jean Moulin d'Albertville accueille la Structure d'Appui Scolaire du Centre National d’Entraînement (SAS CNE) de ski et  de snowboard depuis 1981, réunissant les disciplines de ski alpin, ski freestyle, snowboard. La section permet aux athlètes de préparer leur bac en conciliant l’entraînement sportif et le scolaire.
img: /jaime/images/SHN.jpg
thumb: /jaime/images/SHN.jpg
anim: <b>Pierre POIRIER</b><br/>Professeur de Lettres<br/>Académie de Grenoble
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;salle 210
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
