---
template: jam-theme/jam.html
title: Scénariser ses parcours avec la méthode ABC Learning Design
description: Scénariser c’est adopter une approche centrée sur l’activité des élèves plutôt qu’une approche centrée sur les contenus à apprendre. Le processus de scénarisation invite à sans cesse se poser la question<br/>« Qu’est-ce que les élèves vont faire pour apprendre dans cette étape-ci du cours ? » .
img: /jaime/images/ABCld.jpg
thumb: /jaime/images/ABCld.jpg
anim: <b>Frédérique BESNARD</b><br/>Cheffe de projet local Éléa<br/>Région académique AuRA<br/><br/><b>François LACOUR</b><br/>Chargé de projet Éléa<br/>DRANE Ac-Lyon
horaire: <b>14H30</b>&nbsp;&nbsp;&nbsp;salle 208
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
