---
template: jam-theme/jam.html
title: Premiers pas avec Éléa, mon premier parcours
description: Nous proposons une visite guidée des possibilités offertes et passerons en revues les différentes formes de cours, ainsi que la manière dont ELEA nous donne accès aux outils pour construire un premier parcours, créer des activités simples comme QCM, Appariement et Millionnaire.
img: /jaime/images/initiation.png
thumb: /jaime/images/initiation.png
anim: <b>Johann MOURLON</b><br/>Formateur ÉLÉA<br/>Professeur d’allemand
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;salle 209
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
