---
template: jam-theme/jam.html
title: De moodle à Éléa, changement de pratiques
description: Comment basculer ses pratiques d’un moodle classique à Éléa ?<br/>Quelles modification dans la scénarisation, dans le contenu, dans le guidage ?
img: /jaime/images/Moodle2Elea.jpg
thumb: /jaime/images/Moodle2Elea.jpg
anim: <b>Régis GARNIER</b><br/>Formateur ÉLÉA<br/>Professeur de Mathématiques
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;salle 208
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
