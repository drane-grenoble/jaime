---
template: jam-theme/jam.html
title: Favoriser l’engagement des élèves
description: Tester un parcours "jeu de piste" conçu sur la plateforme Éléa.<br>Découvrir les outils disponibles dans Éléa pour favoriser l'engagement des élèves.
presentation: <iframe width="560" height="315" src="https://codimd.apps.education.fr/p/HmqHyTa9L#/" frameborder="0" allowfullscreen></iframe>
img: /jaime/images/engagement.jpg
thumb: /jaime/images/engagement.jpg
anim: <b>Céline CHAMBON</b><br/>Formatrice ÉLÉA<br/>Professeure de Physique-Chimie
horaire: <b>15H30</b>&nbsp;&nbsp;&nbsp;salle 137
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
