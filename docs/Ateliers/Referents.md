---
template: jam-theme/jam.html
title: Impulser Éléa dans son établissement
description: Cet atelier a pour objectif de réfléchir aux stratégies et outils pour promouvoir l'utilisation efficace de la plateforme Elea au sein de leur établissement. Les participants échangeront autour des méthodes pour susciter l'adhésion des collègues, favoriser la collaboration, et encourager l'intégration de Elea dans la culture pédagogique globale de l'établissement.
img: /jaime/images/referent.jpg
thumb: /jaime/images/referent.jpg
anim: <b>Alexandre JAMET</b><br/>Formateur ÉLÉA<br/>Professeur de SES
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;salle 208
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
