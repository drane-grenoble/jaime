---
template: jam-theme/jam.html
title: Éléa, un parcours pas à pas
description: Présentation de la démarche pour la création du parcours (objectifs pédagogiques, modalités, guidages, choix des tâches, suivi pédagogique, partage aux autres enseignants).<br/>Pas à pas pour des enseignants qui n'ont jamais conçu de parcours.<br/>Retour d’expériences
presentation: <iframe title="JAIME Éléa, le réseau des concepteurs" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/8c440aba-c622-4559-8967-472dbb5f38fa" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
img: /jaime/images/concevoir.png
thumb: /jaime/images/concevoir.png
anim: <b>Sylvain HAUSARD</b><br/>Professeur d’Histoire-Géographie<br/>Académie de Grenoble
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;salle 209
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
