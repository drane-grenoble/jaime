---
template: jam-theme/jam.html
title: Impulser Éléa dans sa discipline
description: L'atelier "Impulser Elea dans sa discipline" vise à explorer des solutions, des stratégies et des outils spécifiques pour promouvoir et intégrer la plateforme Elea dans la culture pédagogique des enseignants. Les participants auront l'occasion de d'échanger autour des défis et des opportunités liés à l'utilisation de Elea dans leur domaine d'enseignement, pour développer des approches adaptées qui favorisent l'adoption et l'exploitation optimale de Elea dans une matière.
img: /jaime/images/discipline.jpg
thumb: /jaime/images/discipline.jpg
anim: <b>Alexandre JAMET</b><br/>Formateur ÉLÉA<br/>Professeur de SES
horaire: <b>14H30</b>&nbsp;&nbsp;&nbsp;salle 208
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
