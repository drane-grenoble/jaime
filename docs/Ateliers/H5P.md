---
template: jam-theme/jam.html
title: Créer des activités interactives avec H5P
description: H5P est un outil puissant pour créer des contenus interactifs facilement, rapidement et de manière personnalisée, tout en garantissant l'accessibilité et en permettant un suivi des résultats. Nous commencerons pas la création d'activités uniques puis d'activités plus complexes et finirons par utiliser une ia générative pour la création de questions.
img: https://www.tituslearning.com/wp-content/uploads/2016/12/H5P-Logo.jpg
thumb: https://www.tituslearning.com/wp-content/uploads/2016/12/H5P-Logo.jpg
anim: <b>Régis GARNIER</b><br/>Formateur ÉLÉA<br/>Professeur de Mathématiques
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;salle 137
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
