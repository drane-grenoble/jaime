---
template: jam-theme/jam.html
title: les sources de simplicité introduites dans Éléa
description: L'objectif d'Éléa est de simplifier une plateforme Moodle sans la dénaturer. Découvrez les éléments qui sont mis en place pour y arriver &#58;<ul><li>une ergonomie revisitée</li><li>des plug-ins pour s'adapter au fonctionnement des écoles, collèges et lycées</li><li>Des activités engageantes pour les élèves et simples à créer pour les enseignants</li></ul>
presentation: <iframe title="JAIME Éléa, source de simplicité" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/c1e63231-2ddf-497d-b55c-6858c6b9cfe3" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe><iframe width="560" height="380" src="/jaime/diaporamas/jaime_simplicite.pdf" frameborder="0" allow="fullscreen"></iframe>
img: /jaime/images/simplicite.jpg
thumb: /jaime/images/simplicite.jpg
anim: <b>Bertrand CHARTIER</b><br/>Chargé de projets Éléa<br/>DRANE AC-Grenoble
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;amphi
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
