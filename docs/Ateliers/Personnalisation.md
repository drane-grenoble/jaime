---
template: jam-theme/jam.html
title: Personnaliser les parcours et les apprentissages
description: <ul><li>Découvrir des exemples pour différencier son enseignement avec Éléa</li><li>Tester un parcours différencié conçu sur la plateforme Éléa</li><li>Des pistes pour différencier son enseignement avec la plateforme.  
img: /jaime/images/personnalisation.jpg
thumb: /jaime/images/personnalisation.jpg
anim: <b>Johann MOURLON</b><br/>Formateur ÉLÉA<br/>Professeur d’allemand
horaire: <b>14H30</b>&nbsp;&nbsp;&nbsp;salle 209
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
