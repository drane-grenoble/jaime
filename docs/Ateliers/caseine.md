---
template: jam-theme/jam.html
title: Caseine en lycée &#58; une plateforme pour l’apprentissage des sciences informatiques
description: Caseine.org est à la fois une plateforme d'apprentissage Moodle et une communauté d’enseignants gratuite et ouverte. Avec des cours dans une quinzaine d'universités, autant de lycées, et plus de 10 000 utilisateurs annuels, son objectif est de favoriser l'apprentissage et l'autonomie des étudiants tout en améliorant la qualité du temps que l'enseignant leur consacre. Sur une même plateforme, caseine s’appuie sur 3 piliers qui mis ensemble font son originalité &#58; <ul><li>- évaluer automatiquement les productions des apprenants, code informatique, modèles mathématiques, etc.<li></li> - assurer un suivi pédagogique des étudiants,<li></li> - partager des contenus entre les enseignants à travers des outils de partage, des cours visitables, des contenus et une communauté d'utilisateurs.</li></ul> Nous présenterons quelques outils pour la pédagogie développés par les membres de la plateforme et mis au service de tous (partage entre enseignants, point de vue des étudiants et des enseignants sur les activités...) ainsi que des fonctionnalités particulièrement utiles fournies par Moodle (connexion LTI avec Elea par exemple). Nous montrerons comment la communauté enseignante s'est approprié ces outils pour proposer des contenus riches et adaptés aux étudiants.
img: https://moodle.caseine.org/pluginfile.php/1/core_admin/logocompact/300x300/1699264079/lait%20caseine.png
thumb: https://moodle.caseine.org/pluginfile.php/1/core_admin/logocompact/300x300/1699264079/lait%20caseine.png
anim: <b>Aurélie LAGOUTTE</b><br/>Maître de Conférences<br/>laboratoire G-SCOP (UGA)<br/><br/><b>Florence THIARD</b><br/>Ingénieure<br/>UGA
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;salle 210
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
short : une plateforme d'apprentissage
---
