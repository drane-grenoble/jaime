---
template: jam-theme/jam.html
title: LabNbook &#58; cahiers de laboratoire et comptes-rendus de TP
img: https://labnbook.fr/wp-content/uploads/2018/05/LabNbook.png
thumb: https://labnbook.fr/wp-content/uploads/2018/05/LabNbook.png
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
anim: <b>Cédric d’Ham</b><br/>Maître de conférences<br/>Laboratoire d’Informatique de Grenoble (UGA)
horaire: <b>12H</b>&nbsp;&nbsp;&nbsp;salle 211
description: LabNbook est une plateforme en ligne dédiée à l'apprentissage des sciences expérimentales, accessible via Moodle. Elle offre aux enseignants la possibilité de concevoir des travaux pratiques en organisant des espaces d'écriture tout en mettant à leur disposition des outils avancés pour la production d'écrits scientifiques, tels que des éditeurs de textes, équations, schémas et protocoles, des tableaux de données associés à un dispositif de modélisation, un éditeur de code python.<br/> Grâce à ses fonctionnalités de collaboration, de suivi et de rétroaction, LabNbook facilite le travail en équipe et le processus itératif. Un module dédié de grilles critériées est également disponible, permettant une évaluation précise des compétences acquises par les élèves.<br/> Au cours de l'atelier, les participants auront l'opportunité d'explorer des activités de travaux pratiques afin de se familiariser avec les fonctionnalités de la plateforme. De plus, ils auront l'occasion d'échanger avec les membres de l'équipe LabNbook sur les défis pédagogiques liés à l'enseignement des sciences expérimentales.
---
