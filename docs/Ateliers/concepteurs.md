---
template: jam-theme/jam.html
title: Réseau des concepteurs, le wikipedia des ressources pédagogiques
description: La plateforme « Réseau des concepteurs » sera disponible en fin d’année 2024. Commune à la France entière, elle permettra aux enseignants, aux formateurs de travailler en équipe. Ils pourront co-construire et maintenir des parcours pédagogiques qui constitueront une bibliothèque de ressources sous licence libre. Tous les enseignants pourront alors, en un clic, mettre se réapproprier ces ressources et les mettre à disposition de leurs élèves.
img: /jaime/images/rdc.jpg
thumb: /jaime/images/rdc.jpg
anim: <b>Rémi LEFEUVRE</b><br/>Chef de projet plateformes e-learning<br/>Ministère de l'Éducation Nationale et de la Jeunesse
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;amphi
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
