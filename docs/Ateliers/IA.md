---
template: jam-theme/jam.html
title: L’Intelligence Artificielle, Éléa et moi
description: Les possibilités offertes par l'intelligence artificielle se développent fortement. Comment peut-on les utiliser pour aider à concevoir des parcours ? Quelles autres possibilités d'utilisation sont envisageables ? Découvrez des cas d'usages concrets et des outils pour utiliser ces algorithmes.
img: /jaime/images/ia.jpg
thumb: /jaime/images/ia.jpg
presentation: <iframe title="JAIME Éléa, L'intelligence Artificielle, Éléa et moi" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/b4a5db96-b681-4618-a013-3c567a1de725" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe><iframe width="560" height="380" src="/jaime/diaporamas/jaime_ia.pdf" frameborder="0" allow="fullscreen"></iframe>
anim: <b>Bertrand CHARTIER</b><br/>Chargé de projets Éléa<br/>DRANE AC-Grenoble
horaire: <b>15H30</b>&nbsp;&nbsp;&nbsp;amphi
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
