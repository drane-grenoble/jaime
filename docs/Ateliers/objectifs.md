---
template: jam-theme/jam.html
title: Apprendre de ses erreurs &#58; permettre aux élèves d’améliorer leurs productions pour progresser
description: Exemples d’usages de Moodle pour proposer des activités variées aux élèves ; guider leur travail en classe et à la maison ; leur apporter des conseils pour progresser ; évaluer leur niveau de connaissances, de compréhension, d’expression et d’implication (dans les 3 langages &#58; écrit, graphique, oral).
img: /jaime/images/competences.jpg
thumb: /jaime/images/competences.jpg
anim: <b>Bruno LEBRAT</b><br/>Professeur d’Histoire-Géographie<br/>Académie de Grenoble
horaire: <b>11H</b>&nbsp;&nbsp;&nbsp;salle 208
dane_link: https://dane.web.ac-grenoble.fr
acad_link: https://ac-grenoble.fr
---
