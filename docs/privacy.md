---
title: Vie Privée
template: jam-theme/full.html
hide:
- navigation
- toc
---


## Données personnelles

La déléguée à la protection des données (DPO) nommée par l'académie de Grenoble peut être contactée par email à dpd@ac-grenoble.fr

### Transfert des données hors de l'Union européenne

Aucun transfert des données hors de l’Union européenne n’est réalisé.

### Droit des personnes

Conformément aux dispositions légales et réglementaires applicables, en particulier la _loi n° 78-17 du 6 janvier modifiée relative à l’informatique, aux fichiers et aux libertés_ et le _règlement européen n° 2016/679/UE du 27 avril 2016_ (applicable depuis le 25 mai 2018), vous disposez des droits suivants :

*   Exercer votre droit d’accès, pour connaître les données personnelles qui vous concernent ;
*   Demander la mise à jour de vos données, si elles sont inexactes ;
*   Demander la portabilité ou la suppression de vos données ;
*   Demander la limitation du traitement de vos données ;
*   Vous opposer, pour des motifs légitimes, au traitement de vos données.

### Droit d’introduire une réclamation

Si vous estimez, après nous avoir contactés, que vos droits Informatique et Libertés ne sont pas respectés, vous avez la possibilité d’introduire une réclamation en ligne auprès de la CNIL ou par courrier postal.

### Cookies et témoins de connexion

Lorsqu’une personne visite le site [drane-grenoble.forge.apps.education.fr/jaime](https://drane-grenoble.forge.apps.education.fr/jaime/), des cookies ou témoins de connexion sont susceptibles d’être déposés sur son terminal (ordinateur, mobile, tablette). Cette page vous permet de mieux comprendre comment fonctionnent les cookies et comment utiliser les outils actuels afin de les paramétrer.

#### Définition

Un "cookie" est un fichier texte susceptible d’être déposé sur votre terminal à l’occasion de la visite de notre site. Il a pour but de collecter des informations d’état, anonymes, relatives à votre navigation, et de vous adresser des contenus adaptés à votre terminal ou à vos centres d’intérêt. Seul l'émetteur d'un cookie est susceptible de lire ou de modifier des informations qui y sont contenues.

#### Finalités des cookies

Les cookies que nous émettons permettent :

- d'établir des statistiques et volumes de fréquentation et d'utilisation des divers éléments composant notre site (nombre de pages vues, nombre de visites, activité, fréquence de retour, etc.) afin de suivre et améliorer la qualité de nos services
- d'adapter la présentation de notre site aux préférences d'affichage de votre terminal (langue, résolution d’affichage, système d’exploitation, etc.)
- de vous faciliter l’utilisation de notre site, par exemple en mémorisant des données relatives à un formulaire ou à des informations que vous avez choisies sur notre site afin de vous fournir du contenu en rapport avec votre intérêt pour les activités de la Drane Académie de Grenoble.

#### Analytiques

Nous utilisons les cookies analytiques uniquement à des fins de recherche interne sur la manière dont nous pouvons améliorer le service que nous offrons à tous nos utilisateurs. Ces cookies permettent simplement d’évaluer la manière dont vous interagissez avec nos sites web — en tant qu’utilisateur anonyme (les données recueillies ne permettent pas de vous identifier personnellement).

