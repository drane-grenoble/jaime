---
title: Mentions Légales
template: jam-theme/full.html
hide:
- navigation
- toc
---

## Informations légales


## Protection des données à caractère personnel (RGPD)


## Contenu du site


Propriété intellectuelle
------------------------

les vidéos et les fichiers PDF qui apparaissent ou sont disponibles sur le site, sont protégés par le droit de la propriété intellectuelle. À ce titre, vous vous engagez à ne pas copier, traduire, reproduire, vendre, publier, exploiter et diffuser des contenus du site protégés par le droit de la propriété intellectuelle, sans autorisation préalable et écrite de l'académie de Grenoble.
Les textes, les images et l'architecture de ce site sont publiés sous licence "MIT License".

les logos de la DRANE Grenoble, de l'académie de Grenoble et de la Région académique Auvergne-Rhônes-Alpes présents sur le site sont la propriété de la région Académique Auvergne-Rhône-Alpes. Sauf autorisation préalable et écrite, l'internaute s'interdit de les utiliser.